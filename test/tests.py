import unittest
from src.my_math import *


class ExampleTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_minus_one_multiply(self):
        self.assertEqual(my_multiply(1, 1), 0)
        self.assertEqual(my_multiply(1, 2), 1)
        self.assertEqual(my_multiply(2, 3), 4)

    if __name__ == "__main__":
        unittest.main()
